{%- from "redis/map.jinja" import redis with context %}


{##

{% if grains['os'] == 'Debian' %}
removing redis pkg in debian:
  file.replace:
    - name: /var/lib/dpkg/statoverride
    - pattern: redis redis 640 /etc/redis/redis.conf
    - repl: ""
    - append_if_not_found: false

removing redis-sentinel pkg in debian:
  file.replace:
    - name: /var/lib/dpkg/statoverride
    - pattern: redis redis 640 /etc/redis/sentinel.conf
    - repl: ""
    - append_if_not_found: false

remove empty line from statoverride file:
  cmd.run:
    - name: "sed -i '/^$/d' /var/lib/dpkg/statoverride"

{% endif %}
##}

remove redis-server pkg:
  pkg.removed:
    - name: {{ redis.package }}

remove redis-tools pkg:
  pkg.removed:
    - name: redis-tools

remove libjemalloc1 pkg:
  pkg.removed:
    - name: libjemalloc1

remove redis-server config file:
  file.absent:
    - name: {{ redis.server.confile }}

disable redis-server service:
  service.dead:
    - name: {{ redis.server.service }}

/etc/systemd/system/redis.service:
  file.absent

/etc/redis:
  file.absent

/var/log/redis:
  file.absent

/var/lib/redis:
  file.absent

{{ redis.server.service_user}}:
  user.absent
