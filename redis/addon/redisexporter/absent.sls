{%- from "redis/map.jinja" import redis with context %}

disable redis_exporter service:
  service.dead:
    - name: {{ redis.exporter.service.name }}

/etc/systemd/system/redis_exporter.service:
  file.absent

/usr/bin/redis_exporter:
  file.absent

{{ redis.exporter.service.user }}:
  user.absent
