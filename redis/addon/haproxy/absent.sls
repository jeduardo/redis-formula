disable haproxy:
  service.dead:
    - name: haproxy

removin haproxy from local node:
  pkg.removed:
    - name: haproxy

/etc/haproxy/haproxy.cfg:
  file.absent

{% if grains['os'] == 'Debian' %}
cleaner for haproxy:
  cmd.run:
    - name: "apt -y autoremove"
{% endif %}
