shipping haproxy local reverse:
  file.managed:
  - name: /etc/haproxy/haproxy.cfg
  - source: salt://redis/addon/haproxy/files/nxcdepsol_lay7checkformaster
  - makedirs: True
  - user: root
  - group: root
  - mode: 755
  - backup: minion

enabling haproxy service:
  service.running:
    - enable: True
    - name: haproxy
    - requires:
      - user: haproxy
