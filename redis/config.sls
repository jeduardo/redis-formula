{%- from "redis/map.jinja" import redis with context %}

deploy redis group:
  group.present:
    - name: redis
    - system: True

deploy redis user:
  user.present:
    - name: redis
    - fullname: Redis system user
    - system: True
    - home: /var/lib/redis
    - shell: /bin/false
    - gid_from_name: redis
    - require:
      - group: redis

deploy redis config for the first time:
  file.managed:
    {% if grains['os'] == 'Debian' %}
    - name: {{ redis.server.confile }}
    {% endif %}
    {% if grains['os'] == 'CentOS' %}
    - name: /etc/redis.conf
    {% endif %}
    - source: salt://redis/files/redis/redis.conf
    - makedirs: True
    - template: jinja
    - user: {{ redis.server.service_user }}
    - group: {{ redis.server.service_group }}
    - mode: 640
    - backup: minion
    - makedirs: True
    - context:
        new_deployment: True
    {#- Deploy a salt-managed config only if the config file is not present or if the config file was not yet Saltified #}
    - onlyif: "! test -f {{ redis.server.confile }} || ! grep Salt {{ redis.server.confile }} 2>&1 1>/dev/null"
    - require:
      - user: redis

update redis config:
  file.blockreplace:
    - name: {{ redis.server.confile }}
    - marker_start: "### Managed by Salt - BEGIN ###"
    - marker_end: "### Managed by Salt - END ###"
    - sources:
      - salt://redis/files/redis/redis.conf
    - onlyif: "grep Salt {{ redis.server.confile }} 2>&1 1>/dev/null"
    - require:
      - user: redis
