{%- from "redis/map.jinja" import redis with context %}

{% if grains['os'] == 'Debian' %}
removing redis-sentinel in debian pkg:
  file.replace:
    - name: /var/lib/dpkg/statoverride
    - pattern: redis redis 640 /etc/redis/sentinel.conf
    - repl: ""
    - append_if_not_found: False

remove empty line from statoverride file in sentinel case:
  cmd.run:
    - name: "sed -i '/^$/d' /var/lib/dpkg/statoverride"

{% endif %}


disable sentinel server service:
  service.dead:
    - name: {{ redis.sentinel.service }}

/lib/systemd/systemd/redis-sentinel.service:
  file.absent

{{ redis.sentinel.server.confile }}:
  file.absent


{{ redis.sentinel.service.service_user }}:
  user.absent
