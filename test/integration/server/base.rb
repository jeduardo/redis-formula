
control "redis part" do
  impact 1.0
  title "Redis Server Service Standalone"
  desc "Check for Redis specified service and file settings"


  describe user('redis') do
    it { should exist }
    its('uid') { should eq 12345 }
    its('gid') { should eq 12345 }
    its('group') { should eq 'redis' }
    its('home') { should eq '/var/lib/redis' }
    its('shell') { should eq '/bin/false' }
  end

  describe group('redis') do
    it { should exist }
  end

  describe file('/etc/redis/redis.conf') do
    its('content') { should match /port 6379/ }
    its('content') { should match /daemonize yes/ }
  end

  describe file('/var/log/redis') do
   its('type') { should eq :directory }
   it { should be_directory }
  end

  describe file('/etc/redis/redis.conf') do
   it { should exist }
   it { should be_owned_by 'redis' }
   its('mode') { should cmp '0640' }
  end

  describe service('redis-server') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end

end


control "Redis addon redis_exporter" do
  impact 1.0
  title "Redis addon redis_exporter"
  desc "Check for Redis redis_exporter"

  describe user('redisexporter') do
    it { should exist }
    its('uid') { should eq 2727 }
    its('gid') { should eq 2727 }
    its('group') { should eq 'redisexporter' }
    its('home') { should eq '/home/redis_exporter' }
    its('shell') { should eq '/bin/false' }
    end

  describe file('/usr/bin/redis_exporter') do
   it { should exist }
   it { should be_owned_by 'root' }
 end

  describe file('/lib/systemd/system/redis_exporter.service') do
   it { should exist }
   it { should be_owned_by 'root' }
 end

  describe file('/etc/redis/redis.conf') do
    its('content') { should match /port 6379/ }
    its('content') { should match /daemonize yes/ }
  end

  describe service('redis_exporter') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
end


control "Redis system settings" do
  impact 1.0
  title "Redis system settings"
  desc "Check for Redis system settings"


  describe command('sysctl vm.overcommit_memory /') do
    its(:stdout) { should match /1/ }
  end

  describe command('sysctl vm.nr_hugepages /') do
    its(:stdout) { should match /vm.nr_hugepages\ =\ 0/ }
  end

  describe command('sysctl net.core.somaxconn /') do
    its(:stdout) { should match /net.core.somaxconn = 65535/ }
  end

end


control "Redis dynomite proxy" do
  impact 1.0
  title "Redis dynomite proxy"
  desc "Check for Redos dynomite proxy"

  describe file('/usr/local/sbin/dynomite') do
   it { should exist }
   it { should be_owned_by 'root' }
  end

  describe file('/lib/systemd/system/dynomite.service') do
   it { should exist }
   it { should be_owned_by 'root' }
  end

  describe service('dynomite') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
end
